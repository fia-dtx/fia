# -*- Coding: UTF-8 -*-
# coding: utf8
import requests
from lxml import html



def titulo():
    x = "Frutas para o pré-treino perfeitas para quem se exercita com frequência:"

    dica =  x
    return dica

def dica1():
    y = "1. Banana concede energia para o treino"

    dica1 = y
    return dica1

def dica2():
    q = "2. Abacate auxilia na perda de gordura abdominal e concede energia"
   
    dica2 = q
    return dica2

def dica3():
    w = "3. Uva gera energia de forma rápida, é pouco calórica e rica em antioxidantes"

    dica3 = w
    return dica3

def dica4():
    e = "4.Melancia garante uma boa circulação sanguínea durante o treino e mantém o corpo hidratado"

    dica4 = e
    return dica4

def dica5():
    r = "5. Laranja concede energia, ajuda a hidratar e é fonte de antioxidantes"

    dica5 = r
    return dica5