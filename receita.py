# -*- Coding: UTF-8 -*-
# coding: utf8
import requests
from lxml import html



def receita():
    x = "TORTA INTEGRAL DE ATUM:"
    
    receita =  x
    return receita 


def ingredientes():
    y = "2 ovos; 1 xícara de leite; 2 xícara de trigo integral; 1/2 xícara de óleo de girassol; 1 xícara de aveia fina; 1 colher (sopa) de fermento; sal a gosto; | 1 cebola picada; 1 tomate picado; 2 ovos cozidos picados; 2 lata de atum natural moído; 1 colher de cheiro-verde; sal e pimenta a gosto"

    ingredientes = y
    return ingredientes

def cfazer():
    c = "*MODO DE PREPARO*"

    cfazer = c
    return cfazer

def finalizacao():
    d = "Coloque todos os ingredientes no liquidificador menos o fermento e bata tudo até ficar bem homogêneo.Depois acrescente o fermento e bata rapidamente só para misturar a massa com o fermento. Misture todos os ingredientes numa tigela e mexa ate todos se encorporarem. Em uma forma média unte com azeite coloque metade da massa espalhe o recheio todo por ela, acrescente o restante da massa, polvilhe se quiser orégano por cima para dar um gostinho especial. Leve ao forno médio por mais ou menos 40 a 45 minutos."

    finalizacao = d
    return finalizacao