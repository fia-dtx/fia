from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium import webdriver
from bs4 import BeautifulSoup
from datetime import datetime
from lxml import html
import selenium
import random
import time

import respostas
import clima
import dica

comando1 = 'clima'.lower()  
comando2 = 'dica'.lower() 
comando3 = 'ola' or 'oi'.lower()
comando4 = 'Bom dia'.lower()
comando5 = 'Boa tarde'.lower()
comando6 = 'Boa noite'.lower()

def verificarMensagem():

    while True:
        a = ActionChains(driver)
        a.key_down(Keys.SHIFT).send_keys(Keys.ENTER).key_up(Keys.SHIFT)

        conversaNova = driver.find_elements_by_class_name('_2WP9Q')
        
        for conversa in conversaNova:

            conversaTratada = conversa.get_attribute("outerHTML")
            if 'P6z4j' in conversaTratada:
                conversa.click()
                mensagem = conversa.find_element_by_class_name('_1Wn_k').text
                caixaDeTexto = driver.find_element_by_xpath('//*[@id="main"]/footer/div[1]/div[2]/div/div[2]')
                time.sleep(2)
                print(mensagem)
                
                if comando1 == mensagem.lower():

                    caixaDeTexto.send_keys("Pompeia - " +clima.clima() + " Graus")
                    caixaDeTexto.send_keys(Keys.ENTER)

                elif mensagem.find("dica" or "dicas") == mensagem.lower():

                    caixaDeTexto.send_keys(dica.titulo())
                    caixaDeTexto.send_keys(Keys.ENTER)
                    caixaDeTexto.send_keys(dica.dica1())
                    caixaDeTexto.send_keys(Keys.ENTER)
                    caixaDeTexto.send_keys(dica.dica2())
                    caixaDeTexto.send_keys(Keys.ENTER)
                    caixaDeTexto.send_keys(dica.dica3())
                    caixaDeTexto.send_keys(Keys.ENTER)
                    caixaDeTexto.send_keys(dica.dica4())
                    caixaDeTexto.send_keys(Keys.ENTER)
                    caixaDeTexto.send_keys(dica.dica5())
                    caixaDeTexto.send_keys(Keys.ENTER)
                
                elif mensagem.find("ola" or "oi") == mensagem.lower():
                    caixaDeTexto.send_keys(random.choice(respostas.greetings))
                    caixaDeTexto.send_keys(Keys.ENTER)

                elif comando4 == mensagem.lower():
                    caixaDeTexto.send_keys("Bom dia!")
                    caixaDeTexto.send_keys(Keys.ENTER)
                
                elif comando5 == mensagem.lower():
                    caixaDeTexto.send_keys("Boa tarde!")
                    caixaDeTexto.send_keys(Keys.ENTER)

                elif comando6 == mensagem.lower():
                    caixaDeTexto.send_keys("Boa tarde!")
                    caixaDeTexto.send_keys(Keys.ENTER)

        else:
                main =["Eu"]
                main = driver.find_element_by_xpath(f"//span[@title='Eu']")
                time.sleep(3)
                main.click()

                
def abreChrome():
    
    global driver

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("user-data-dir=selenium") 
    prefs = {"profile.default_content_setting_values.notifications" : 2}
    chrome_options.add_experimental_option("prefs",prefs)
    driver = webdriver.Chrome()
    driver.get('https://web.whatsapp.com')


abreChrome()
time.sleep(10)
verificarMensagem()

driver.quit()